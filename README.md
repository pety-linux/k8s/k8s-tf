# Intro
This CICD Pipeline provides the Terraform code for Deployment of Resources on AWS.
It can be used e.g. for Deployment of Your K8S cluster.

## Structure of repo
- terraform folder contains Terraform code
- .gitlab-ci. yml contains logis for deployment

## Scope of AWS Resources
- vpc
- igw
- natgw
- rt 
- sg 
- ec2 
- route53

## How to use it
In order to use this Pipeline you just need to provide the valid AWS credentials (e.g. taken from aCloudguru).
```
Settings -> CI/CD -> Variables 
```
```
TF_VAR_aws_access_key
TF_VAR_aws_region
TF_VAR_aws_sec_key
```
## Limits
- max 10 EC2 at once
