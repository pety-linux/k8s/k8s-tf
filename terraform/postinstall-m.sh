#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Create User
useradd -s /bin/bash -c "Admin" -m admin
echo "Passw0rd" | passwd --stdin admin
# Set sudo
echo "admin ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/admin/.ssh
cat <<EOF | tee -a /home/admin/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R admin /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
##

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

## K8S
yum -y install git jq
# Clone Repo
git clone https://gitlab.com/pety-linux/k8s/k8s-install.git
# Run K8S Installation Script
bash k8s-install/install-node-containerd.sh
##

## AWS Route53 stuff
# Variables
ip=`hostname -i`
zoneid=`aws route53 list-hosted-zones-by-name |  jq --arg name "pety.net." -r '.HostedZones | .[] | select(.Name=="\($name)") | .Id' | awk -F / '{ print $1 $3}'`
# Prepare config
cat <<EOF | tee dns.json
{
            "Comment": "CREATE/DELETE/UPSERT a record ",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                                    "Name": "master.pety.net",
                                    "Type": "A",
                                    "TTL": 300,
                                 "ResourceRecords": [{ "Value": "${ip}"}]
}}]
}
EOF
# Apply
aws route53 change-resource-record-sets --hosted-zone-id ${zoneid} --change-batch file://dns.json
#

## Web server
yum -y install httpd
systemctl enable httpd --now

## Init K8S Cluster
kubeadm init --pod-network-cidr=10.244.0.0/16 | tee -a command
cat command |grep -A1 ^kubeadm > initcommand
cat initcommand  | bash
mv initcommand /var/www/html
rm command
# kubeconfig
mkdir -p /root/.kube
cp -i /etc/kubernetes/admin.conf /root/.kube/config
export KUBECONFIG=/etc/kubernetes/admin.conf
# CNI
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Install kubens
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
tar xvzf kubens_v0.9.4_linux_x86_64.tar.gz
mv kubens /usr/local/bin/
##


## Ingress Controller
#kubectl create ns nginx-ingress
#helm repo add nginx-stable https://helm.nginx.com/stable
#helm repo update
#helm install nginx-ingress nginx-stable/nginx-ingress --set controller.service.loadBalancerIP=$LBIP --namespace nginx-ingress

#wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/baremetal/deploy.yaml
#sed -i "s/NodePort/LoadBalancer/g" deploy.yaml
#sed -i "/LoadBalancer/a\  externalIPs:\n    - $LBIP" deploy.yaml
#kubectl apply -f deploy.yaml
##

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k=kubectl
alias c=clear
EOF
##
