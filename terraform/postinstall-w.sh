#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Create User
useradd -s /bin/bash -c "Admin" -m admin
echo "Passw0rd" | passwd --stdin admin
# Set sudo
echo "admin ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/admin/.ssh
cat <<EOF | tee -a /home/admin/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R admin /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
##

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

## K8S
yum -y install git jq
# Clone Repo
git clone https://gitlab.com/pety-linux/k8s/k8s-install.git
# Run K8S Installation Script
bash k8s-install/install-node-containerd.sh
#
# Join to the Cluster
sleep 5m
wget -O - http://master.pety.net/initcommand | bash
##
